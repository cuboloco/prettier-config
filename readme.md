# prettier-config

> A [Prettier](https://prettier.io) config.

## Usage

**Install**:

```bash
$ yarn add -D prettier
$ yarn add -D @cuboloco/prettier-config
```

**Edit `package.json`**:

```jsonc
{
  // ...
  "prettier": "@cuboloco/prettier-config"
}
```
